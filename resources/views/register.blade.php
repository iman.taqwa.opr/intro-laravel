<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sign UP | SanberBook</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
      @csrf
      <label for="fname">First name:</label>
      <br />
      <br />
      <input type="text" name="fname" id="fname" />
      <br />
      <br />
      <label for="lname">Last Name:</label>
      <br />
      <br />
      <input type="text" name="lname" id="lname" />
      <br />
      <br />
      Gender:
      <br />
      <br />
      <input type="radio" name="gender" id="Male" /><label for="Male">Male</label>
      <br />
      <input type="radio" name="gender" id="Female" /><label for="Female">Female</label>
      <br />
      <input type="radio" name="gender" id="Other" /><label for="Other">Other</label>
      <br />
      <br />
      <label for="nationality">Nationality</label>
      <br />
      <br />
      <select name="nationality" id="nationality">
        <option value="indonesian">Indonesian</option>
        <option value="singaporean">Singaporean</option>
        <option value="malaysian">Malaysian</option>
        <option value="australian">Australian</option>
      </select>
      <br />
      <br />
      Language Spoken:
      <br />
      <br />
      <input type="checkbox" id="bahasaindonesia" /><label for="bahasaindonesia">Bahasa Indonesia</label>
      <br />
      <input type="checkbox" id="english" /><label for="english">English</label>
      <br />
      <input type="checkbox" id="other" /><label for="other">Other</label>
      <br />
      <br />
      Bio:
      <br />
      <br />
      <textarea name="bio" cols="30" rows="10"></textarea>
      <br />
      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
